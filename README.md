# video_player_web

The web implementation of [`video_player`][1] with VideoJS library.

## Usage

This package is the endorsed implementation of `video_player` for the web platform since version `0.10.5`, so it gets automatically added to your application by depending on `video_player: ^0.10.5`.

No further modifications to your `pubspec.yaml` should be required in a recent enough version of Flutter (`>=1.12.13+hotfix.4`):

```yaml
...
dependency_overrides:
  video_player_web:
    git: https://gitlab.com/dipdev.studio/open-source/flutter-web-videojs.git

```

Add to your index.html file
```html
  <link href="https://unpkg.com/video.js/dist/video-js.min.css" rel="stylesheet" />
  <script src="https://unpkg.com/video.js/dist/video.min.js"></script>
```