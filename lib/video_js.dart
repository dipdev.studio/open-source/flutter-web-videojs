@JS()
library videojs;

import 'dart:html';

import 'package:js/js.dart';

@JS('videojs')
class VideoJS {
  external factory VideoJS(
      Element element, VideoJSOptions options, [Function ready]);
  @JS()
  external void ready(Function fn, bool sync);
  @JS()
  external num duration();
  @JS()
  external num videoWidth();
  @JS()
  external num videoHeight();
  @JS()
  external set currentTime(num seconds);
  @JS()
  external num get currentTime;
  @JS()
  external set volume(num percentAsDecimal);
  @JS()
  external num get volume;
  @JS()
  external TimeRanges get buffered;
  @JS()
  external set loop(bool value);
  @JS()
  external set aspectRatio(String ratio);
  @JS()
  external String get aspectRatio;
  @JS()
  external VideoElement player();
  @JS()
  external Future<void> play();
  @JS()
  external void pause();
  @JS()
  external bool paused();
  @JS()
  external set src(SourseOptions sourse);
  @JS()
  external void load();
  @JS()
  external void dispose();
   /* @JS()
  external void on(String first, Function second); */

   @JS()
  external static void on(Element element, String first, Function second);

  
}

@JS()
@anonymous
class VideoJSOptions {
  external bool get autoplay;
  external bool get controls;
  external num get height;
  external bool get loop;
  external bool get muted;
  external String get poster;
  external String get preload;
  external String get src;
  external num get width;

  external factory VideoJSOptions(
      {bool autoplay,
      bool controls,
      num height,
      bool loop,
      bool muted,
      String poster,
      String preload,
      String src,
      num width});
}

@JS()
@anonymous
class SourseOptions {
  external String get type;
  external String get src;

  external factory SourseOptions({String type, String src});
}
