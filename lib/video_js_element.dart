import 'dart:html';

class VideoJSElement extends HtmlElement {
  static final tag = 'video-js';
  factory VideoJSElement() => Element.tag(tag);

  VideoJSElement.created() : super.created();
}
